FROM golang:latest

# RUN add-apt-repository
ADD . /app
ENTRYPOINT /app
WORKDIR /app

RUN go get github.com/gin-gonic/gin && go get github.com/jessevdk/go-assets-builder
RUN cd /app && rm -rf build
RUN go-assets-builder templates -o assets.go 
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -a -installsuffix cgo -o main .
CMD ["/app/main"]

#FROM alpine:3.8
FROM scratch
COPY --from=0 /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY --from=0 /app /app/
CMD ["/app/main"]