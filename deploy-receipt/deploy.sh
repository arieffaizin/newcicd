#!/bin/bash

PROJECT=${1}
NAME=${2}
TAG=${3}
URL=${4}

export PROJECT=${PROJECT}
export BUILD_NUMBER=${TAG}
export PROJECT_NAME=${NAME}
export ENV_URL=${URL}

mkdir .generated

for f in recipes/*.yaml
do
 envsubst < $f > ".generated/$(basename $f)"
done

kubectl apply -f .generated/*.yaml
