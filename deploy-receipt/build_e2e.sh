#!/bin/bash

set -e

apt-get install -y curl sudo

curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -
sudo apt-get install -y nodejs

## INSTALL DEP
npm install

## STARTING SERVER
./build/main  > /dev/null 2>&1 &
SERVER_PID=$!

echo "Server has running running in the background at pid ${SERVER_PID}"

## Run end to end testing
./node_modules/.bin/cypress run

## KILLING SERVER
echo "Killing Server [$SERVER_PID] ..."

kill -s TERM $SERVER_PID